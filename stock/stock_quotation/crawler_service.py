
# http://www.bmfbovespa.com.br/pt_br/produtos/listados-a-vista-e-derivativos/renda-variavel/empresas-listadas.htm
from datetime import datetime, timedelta

import requests

import re
from lxml.html import fragment_fromstring
from lxml import html


BASE_URL = 'http://www.fundamentus.com.br/'


class FundamentusCrawler:
    base_url = 'http://www.fundamentus.com.br/'

    def __init__(self):
        self.companies = []

    @property
    def titles(self):
        return ['Nro. Ações', 'P/L', 'LPA', 'P/VP', 'VPA', 'P/EBIT', 'Marg. Bruta', 'PSR', 'Marg. EBIT', 'P/Ativos',
                'Marg. Líquida', 'P/Cap. Giro', 'EBIT / Ativo', 'P/Ativ Circ Liq', 'ROIC', 'Div. Yield', 'ROE',
                'EV / EBIT', 'Liquidez Corr', 'Giro Ativos', 'Div Br/ Patrim', 'Cres. Rec (5a)', 'Ativo', 'Dív. Bruta',
                'Disponibilidades', 'Dív. Líquida', 'Ativo Circulante', 'Patrim. Líq']

    @staticmethod
    def _clear_value(value):
        transformed_value = value.strip('\t\r\n% ').replace('.', '').replace(',', '.')
        if transformed_value == '-':
            return 0
        return float(transformed_value)

    def validate_title(self, title):
        # find a way to validate the title and get the value automatically
        pass

    @staticmethod
    def _get_result_release_date(tree):
        print('validating report release age...')
        release_date_parsed = tree.xpath('//table[2]//tr[1]//td[4]/span[1]/text()')
        if not len(release_date_parsed):
            return None
        release_date = datetime.strptime(release_date_parsed[0], '%d/%m/%Y')
        # # if release_date + timedelta(days=365) < datetime.today():
        # #     return None
        return release_date.date()

    @staticmethod
    def _get_last_negotiation_date(tree):
        last_negotiation_date_parsed = tree.xpath('//table[1]//tr[2]//td[4]/span[1]/text()')
        if not len(last_negotiation_date_parsed) or last_negotiation_date_parsed[0] == '-':
            return None
        last_negotiation_date = datetime.strptime(last_negotiation_date_parsed[0], '%d/%m/%Y')
        return last_negotiation_date.date()

    def _get_indicators(self, tree, release_date):
        indicators = {
            # 2 table
            'release_date': release_date,

            'stock_no': self._clear_value(tree.xpath('//table[2]//tr[2]//td[4]/span[1]/text()')[0]),

            # indicadores fundamentalistas
            'pl': self._clear_value(tree.xpath('//table[3]//tr[2]//td[4]/span[1]/text()')[0]),
            'lpa': self._clear_value(tree.xpath('//table[3]//tr[2]//td[6]/span[1]/text()')[0]),
            'pvp': self._clear_value(tree.xpath('//table[3]//tr[3]//td[4]/span[1]/text()')[0]),
            'vpa': self._clear_value(tree.xpath('//table[3]//tr[3]//td[6]/span[1]/text()')[0]),
            'pebit': self._clear_value(tree.xpath('//table[3]//tr[4]//td[4]/span[1]/text()')[0]),
            'mb': self._clear_value(tree.xpath('//table[3]//tr[4]//td[6]/span[1]/text()')[0]),
            'psr': self._clear_value(tree.xpath('//table[3]//tr[5]//td[4]/span[1]/text()')[0]),
            'me': self._clear_value(tree.xpath('//table[3]//tr[5]//td[6]/span[1]/text()')[0]),
            'at': self._clear_value(tree.xpath('//table[3]//tr[6]//td[4]/span[1]/text()')[0]),
            'ml': self._clear_value(tree.xpath('//table[3]//tr[6]//td[6]/span[1]/text()')[0]),
            'pcg': self._clear_value(tree.xpath('//table[3]//tr[7]//td[4]/span[1]/text()')[0]),
            'ebita': self._clear_value(tree.xpath('//table[3]//tr[7]//td[6]/span[1]/text()')[0]),
            'p_ativ_circ_liq': self._clear_value(tree.xpath('//table[3]//tr[8]//td[4]/span[1]/text()')[0]),
            'roic': self._clear_value(tree.xpath('//table[3]//tr[8]//td[6]/span[1]/text()')[0]),
            'div_yield': self._clear_value(tree.xpath('//table[3]//tr[9]//td[4]/span[1]/text()')[0]),
            'roe': self._clear_value(tree.xpath('//table[3]//tr[9]//td[6]/span[1]/text()')[0]),
            'ev_ebit': self._clear_value(tree.xpath('//table[3]//tr[10]//td[4]/span[1]/text()')[0]),
            'liquidez_corr': self._clear_value(tree.xpath('//table[3]//tr[10]//td[6]/span[1]/text()')[0]),
            'giro_ativ': self._clear_value(tree.xpath('//table[3]//tr[11]//td[4]/span[1]/text()')[0]),
            'div_br_patrim': self._clear_value(tree.xpath('//table[3]//tr[11]//td[6]/span[1]/text()')[0]),
            'cresc_rec_5_a': self._clear_value(tree.xpath('//table[3]//tr[12]//td[4]/span[1]/text()')[0]),

            # balanco patrimonial
            'ativo': self._clear_value(tree.xpath('//table[4]//tr[2]//td[2]/span[1]/text()')[0]),
            'div_bruta': self._clear_value(tree.xpath('//table[4]//tr[2]//td[4]/span[1]/text()')[0]),
            'disponibilidades': self._clear_value(tree.xpath('//table[4]//tr[3]//td[2]/span[1]/text()')[0]),
            'div_liquida': self._clear_value(tree.xpath('//table[4]//tr[3]//td[4]/span[1]/text()')[0]),
            'ativo_circulante': self._clear_value(tree.xpath('//table[4]//tr[4]//td[2]/span[1]/text()')[0]),
            'patr_liquido': self._clear_value(tree.xpath('//table[4]//tr[4]//td[4]/span[1]/text()')[0])
        }
        return indicators

    @staticmethod
    def _sanitize_paper_type(value: str):
        if 'ON' in value:
            return 'ON'
        if 'PN' in value:
            return 'PN'
        if 'PNA' in value:
            return 'PNA'
        return '-'

    def _get_base_details(self, tree):
        paper_type = tree.xpath('//table[1]//tr[2]//td[2]/span[1]/text()')
        name = tree.xpath('//table[1]//tr[3]//td[2]/span[1]/text()')
        sector = tree.xpath('//table[1]//tr[4]//td[2]/span[1]/a/text()')
        sub_sector = tree.xpath('//table[1]//tr[5]//td[2]/span[1]/a/text()')
        # first table
        company = {
            'code': tree.xpath('//table[1]//tr[1]//td[2]/span[1]/text()')[0],
            'paper_type': self._sanitize_paper_type(paper_type[0]) if paper_type else '-',
            'name': name[0] if name else '-',
            'sector': sector[0] if sector else '-',
            'sub_sector': sub_sector[0] if sub_sector else '-',
            'last_negotiation_date': self._get_last_negotiation_date(tree),
            'indicators': []
        }
        return company

    def get_fundamentus_detail(self, paper='FESA4'):
        print('crawling fundamentus from {}'.format(paper))
        url = '{}detalhes.php?papel={}'.format(BASE_URL, paper)
        page = requests.get(url)
        tree = html.fromstring(page.text)

        release_date = self._get_result_release_date(tree)
        if release_date is None:
            return None

        company = self._get_base_details(tree)
        indicators = self._get_indicators(tree, release_date)

        company['indicators'].append(indicators)
        return company

    def get_companies_fundamentus_detail(self, papers=None):
        if papers is None:
            papers = []

        companies = []

        for paper in papers:
            company = self.get_fundamentus_detail(paper.code)
            companies.append(company)

        return companies

    @staticmethod
    def get_company_list():
        content = requests.get('{}resultado.php'.format(BASE_URL))
        pattern = re.compile('<table id="resultado".*</table>', re.DOTALL)
        reg = re.findall(pattern, content.text)[0]
        page = fragment_fromstring(reg)
        lista = []

        for rows in page.xpath('tbody')[0].findall("tr"):
            children = rows.getchildren()
            name = children[0][0].getchildren()[0].text
            data = {'code': name}
            lista.append(data)
        return lista
