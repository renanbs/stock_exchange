from datetime import datetime, timedelta

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from stock.stock_quotation.ordering_service import get_company_grade
from stock.stock_quotation.stock_service import AlphaAdvantageStock
from .models import Company, OrderCompany, Stock
from .serializers import CompanySerializer, CompanyCodeSerializer, OrderCompanySerializer, StockSerializer, \
    ResponseCompanySerializer
from .crawler_service import FundamentusCrawler

import django_rq


def _get_company_fundamentus(company=None):
    fc = FundamentusCrawler()
    det = fc.get_fundamentus_detail(company.code)
    if det is not None:
        ss = CompanySerializer(data=det)
        if ss.is_valid():
            ss.update(instance=company, validated_data=det)
        else:
            print('not valid-> [{}] - {}'.format(ss.errors, ss.error_messages))


def _get_companies_fundamentus():
    fc = FundamentusCrawler()
    company_list = Company.objects.all()
    for company in company_list:
        det = fc.get_fundamentus_detail(company.code)
        if det is not None:
            ss = CompanySerializer(data=det)
            if ss.is_valid():
                ss.update(instance=company, validated_data=det)
            else:
                print('not valid-> [{}] - {}'.format(ss.errors, ss.error_messages))
    # fc = FundamentusCrawler()
    # company_list = Company.objects.all()
    # companies = fc.get_companies_fundamentus_detail(company_list)
    # if len(companies) > 0:
    #     ss = CompanySerializer(data=companies, many=True)
    #     if ss.is_valid():
    #         ss.update(instance=companies, validated_data=companies)
    #     else:
    #         print('not valid-> [{}] - {}'.format(ss.errors, ss.error_messages))


class CompanyViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)

    queryset = Company.objects.all()

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            # TODO: change this to return only the last day/week of stock data
            return ResponseCompanySerializer
        return CompanySerializer

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            instance.disable = True
            # only logical delete
            self.perform_update(instance)
        except ObjectDoesNotExist:
            Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_200_OK)

    @action(methods=['get'], detail=False, url_path='get-companies')
    def get_companies(self, request):
        companies = Company.objects.filter(disable=False)

        return Response(data=companies.values())

    @action(methods=['post'], detail=False, url_path='update-company-list')
    def update_company_list(self, request):
        fc = FundamentusCrawler()
        company_list = fc.get_company_list()
        ss = CompanyCodeSerializer(data=company_list, many=True)
        if ss.is_valid():
            ss.save()
            return Response('ok')
        else:
            return Response('nok', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False, url_path='update-companies-fundamentus')
    def update_companies_fundamentus(self, request):
        job = django_rq.enqueue(_get_companies_fundamentus)
        return Response({'job_id': job.get_id()})

    @action(methods=['post'], detail=True, url_path='update-company-fundamentus')
    def update_company_fundamentus(self, request, pk=None):
        company = Company.objects.get(id=pk)
        _get_company_fundamentus(company)

        return Response('ok')

    @staticmethod
    def _save_stock_from_company(company, external_stock_api):
        print('getting stock for company {}:{}:{}'.format(company.id, company.code, company.name))
        stocks, msg = external_stock_api.get_daily_series(symbol=company.code, output_size='full')
        if stocks:
            ss = StockSerializer(data=stocks, many=True)
            if ss.is_valid():
                ss.save(company=company)
            else:
                print('not valid-> [{}] - {}'.format(ss.errors, ss.error_messages))
        return stocks, msg

    @action(methods=['post'], detail=True, url_path='update-stock')
    def update_company_stock(self, request, pk=None):
        company = Company.objects.get(id=pk)
        external_stock_api = AlphaAdvantageStock()
        data, msg = self._save_stock_from_company(company, external_stock_api)
        message = {'company_name': company.name, 'message': msg}
        if not data:
            return Response(message, status=status.HTTP_204_NO_CONTENT)
        return Response(message)

    @action(methods=['get'], detail=False, url_path='update-companies-stock')
    def update_companies_stock(self, request):
        companies = Company.objects.filter(disable=False)
        external_stock_api = AlphaAdvantageStock()
        for company in companies:
            data, msg = self._save_stock_from_company(company, external_stock_api)
            if not data:
                print('{}-{}'.format(data, msg))
        return Response('ok')

    @action(methods=['get'], detail=False, url_path='get-job-status')
    def get_job_status(self, request):
        job_id = request.query_params.get('job-id')
        response = 'no jobs'

        if job_id is not None:
            q = django_rq.get_queue('default')
            job = q.fetch_job(job_id)
            if job is None:
                response = {'status': 'unknown'}
            else:
                response = {
                    'status': job.get_status(),
                    'result': job.result,
                }
                if job.is_failed:
                    response['message'] = job.exc_info.strip().split('\n')[-1]

        return Response(response)

    @action(methods=['delete'], detail=False, url_path='disable-companies')
    def disable_companies(self, request):
        a_year_ago = datetime.today().date() - timedelta(days=365)
        fifteen_days_ago = datetime.today().date() - timedelta(days=15)
        companies = Company.objects.all()
        companies_to_be_disabled = []
        for company in companies:
            if not company.last_indicator:
                continue
            if company.last_indicator.release_date <= a_year_ago:
                companies_to_be_disabled.append(company)
                continue
            if not company.last_negotiation_date or company.last_negotiation_date <= fifteen_days_ago:
                companies_to_be_disabled.append(company)
        for company in companies_to_be_disabled:
            company.disable = True
            # only logical delete
            self.perform_update(company)
        return Response(data=len(companies_to_be_disabled), status=status.HTTP_202_ACCEPTED)


class OrderCompanyViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    queryset = OrderCompany.objects.all()
    serializer_class = OrderCompanySerializer

    @action(methods=['get'], detail=True, url_path='get-companies-in-order')
    def get_companies_in_order(self, request, pk=None):
        try:
            order = OrderCompany.objects.get(pk=pk)
        except ObjectDoesNotExist:
            return Response('order does not exist', status=status.HTTP_404_NOT_FOUND)
        companies = Company.objects.all().exclude(disable=True).exclude(sector="Financeiros")
        companies_grade = []
        for company in companies:
            if company.name:
                print('calculating company grade {}:{}'.format(company.id, company.name))
                indicator = company.last_indicator
                grade = get_company_grade(indicator, order)
                companies_grade.append({'id': company.id, 'code': company.code, 'disabled': company.disable,
                                        'last_negotiation_date': company.last_negotiation_date,
                                        'last_indicator_release_date': company.last_indicator.release_date,
                                        'name': company.name, 'grade': grade, 'pl': indicator.pl})
        sorted_companies_grade = sorted(companies_grade, key=lambda x: x['grade'], reverse=True)
        print(len(companies))
        return Response(sorted_companies_grade)


class StockViewSet(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.DestroyModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    permission_classes = (IsAuthenticated,)

    queryset = Stock.objects.all()
    serializer_class = StockSerializer
    #
    # @action(methods=['get'], detail=True, url_path='get-company-stock')
    # def get_company_stock(self, request, pk=None):
    #     aa = AlphaAdvantageStock()
    #     response = aa.get_daily_series('FESA4', 5)
    #     return Response(response)
