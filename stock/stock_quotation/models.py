import datetime

from django.db import models


class Company(models.Model):
    code = models.CharField("Code", max_length=10, blank=False)
    name = models.CharField("Name", max_length=50, blank=True)
    paper_type = models.CharField("Paper Type", max_length=2, blank=True)
    sector = models.CharField("Sector", max_length=50, blank=True)
    sub_sector = models.CharField("Sub Sector", max_length=100, blank=True)
    last_negotiation_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    disable = models.BooleanField(default=False)

    @property
    def last_indicator(self):
        return FundamentalistIndicator.objects.filter(company=self).latest()

    @property
    def last_stock(self):
        return Stock.objects.filter(company=self).latest()

    @property
    def last_stock_month(self):
        one_month_ago = (datetime.datetime.today() - datetime.timedelta(days=30)).date()
        return Stock.objects.filter(company=self).filter(date__gte=one_month_ago)


class FundamentalistIndicator(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='indicators')
    release_date = models.DateField(auto_now=False, auto_now_add=False)
    stock_no = models.DecimalField(max_digits=30, decimal_places=2)

    pl = models.DecimalField(max_digits=30, decimal_places=2)
    lpa = models.DecimalField(max_digits=30, decimal_places=2)
    pvp = models.DecimalField(max_digits=30, decimal_places=2)
    vpa = models.DecimalField(max_digits=10, decimal_places=2)
    pebit = models.DecimalField(max_digits=30, decimal_places=2)
    mb = models.DecimalField(max_digits=30, decimal_places=2)
    psr = models.DecimalField(max_digits=30, decimal_places=2)
    me = models.DecimalField(max_digits=30, decimal_places=2)
    at = models.DecimalField(max_digits=30, decimal_places=2)
    ml = models.DecimalField(max_digits=30, decimal_places=2)
    pcg = models.DecimalField(max_digits=30, decimal_places=2)
    ebita = models.DecimalField(max_digits=30, decimal_places=2)
    p_ativ_circ_liq = models.DecimalField(max_digits=30, decimal_places=2)
    roic = models.DecimalField(max_digits=30, decimal_places=2)
    div_yield = models.DecimalField(max_digits=30, decimal_places=2)
    roe = models.DecimalField(max_digits=30, decimal_places=2)
    ev_ebit = models.DecimalField(max_digits=30, decimal_places=2)
    liquidez_corr = models.DecimalField(max_digits=30, decimal_places=2)
    giro_ativ = models.DecimalField(max_digits=30, decimal_places=2)
    div_br_patrim = models.DecimalField(max_digits=30, decimal_places=2)
    cresc_rec_5_a = models.DecimalField(max_digits=30, decimal_places=2)

    ativo = models.DecimalField(max_digits=30, decimal_places=2)
    div_bruta = models.DecimalField(max_digits=30, decimal_places=2)
    disponibilidades = models.DecimalField(max_digits=30, decimal_places=2)
    div_liquida = models.DecimalField(max_digits=30, decimal_places=2)
    ativo_circulante = models.DecimalField(max_digits=30, decimal_places=2)
    patr_liquido = models.DecimalField(max_digits=30, decimal_places=2)

    class Meta:
        ordering = ['release_date']
        get_latest_by = 'release_date'

    def __str__(self):
        return '{}'.format(self.release_date)

    def __repr__(self):
        return '{}'.format(self.release_date)


class OrderCompany(models.Model):
    name = models.CharField("Name", max_length=50, blank=True)
    pl = models.DecimalField(max_digits=3, decimal_places=3)
    disable_pl = models.BooleanField(default=False)
    lpa = models.DecimalField(max_digits=3, decimal_places=3)
    disable_lpa = models.BooleanField(default=False)
    pvp = models.DecimalField(max_digits=3, decimal_places=3)
    disable_pvp = models.BooleanField(default=False)
    vpa = models.DecimalField(max_digits=10, decimal_places=3)
    disable_vpa = models.BooleanField(default=False)
    pebit = models.DecimalField(max_digits=3, decimal_places=3)
    disable_pebit = models.BooleanField(default=False)
    mb = models.DecimalField(max_digits=3, decimal_places=3)
    disable_mb = models.BooleanField(default=False)
    psr = models.DecimalField(max_digits=3, decimal_places=3)
    disable_psr = models.BooleanField(default=False)
    me = models.DecimalField(max_digits=3, decimal_places=3)
    disable_me = models.BooleanField(default=False)
    at = models.DecimalField(max_digits=3, decimal_places=3)
    disable_at = models.BooleanField(default=False)
    ml = models.DecimalField(max_digits=3, decimal_places=3)
    disable_ml = models.BooleanField(default=False)
    pcg = models.DecimalField(max_digits=3, decimal_places=3)
    disable_pcg = models.BooleanField(default=False)
    ebita = models.DecimalField(max_digits=3, decimal_places=3)
    disable_ebita = models.BooleanField(default=False)
    p_ativ_circ_liq = models.DecimalField(max_digits=3, decimal_places=3)
    disable_p_ativ_circ_liq = models.BooleanField(default=False)
    roic = models.DecimalField(max_digits=3, decimal_places=3)
    disable_roic = models.BooleanField(default=False)
    div_yield = models.DecimalField(max_digits=3, decimal_places=3)
    disable_div_yield = models.BooleanField(default=False)
    roe = models.DecimalField(max_digits=3, decimal_places=3)
    disable_roe = models.BooleanField(default=False)
    ev_ebit = models.DecimalField(max_digits=3, decimal_places=3)
    disable_ev_ebit = models.BooleanField(default=False)
    liquidez_corr = models.DecimalField(max_digits=3, decimal_places=3)
    disable_liquidez_corr = models.BooleanField(default=False)
    giro_ativ = models.DecimalField(max_digits=3, decimal_places=3)
    disable_giro_ativ = models.BooleanField(default=False)
    div_br_patrim = models.DecimalField(max_digits=3, decimal_places=3)
    disable_div_br_patrim = models.BooleanField(default=False)
    cresc_rec_5_a = models.DecimalField(max_digits=3, decimal_places=3)
    disable_cresc_rec_5_a = models.BooleanField(default=False)

    ativo = models.DecimalField(max_digits=3, decimal_places=3)
    disable_ativo = models.BooleanField(default=False)
    div_bruta = models.DecimalField(max_digits=3, decimal_places=3)
    disable_div_bruta = models.BooleanField(default=False)
    disponibilidades = models.DecimalField(max_digits=3, decimal_places=3)
    disable_disponibilidades = models.BooleanField(default=False)
    div_liquida = models.DecimalField(max_digits=3, decimal_places=3)
    disable_div_liquida = models.BooleanField(default=False)
    ativo_circulante = models.DecimalField(max_digits=3, decimal_places=3)
    disable_ativo_circulante = models.BooleanField(default=False)
    patr_liquido = models.DecimalField(max_digits=3, decimal_places=3)
    disable_patr_liquido = models.BooleanField(default=False)


class Stock(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='stock')
    date = models.DateField(auto_now=False, auto_now_add=False)

    open = models.DecimalField(max_digits=15, decimal_places=3)
    high = models.DecimalField(max_digits=15, decimal_places=3)
    low = models.DecimalField(max_digits=15, decimal_places=3)
    close = models.DecimalField(max_digits=15, decimal_places=3)
    volume = models.PositiveIntegerField(default=0)

    class Meta:
        get_latest_by = 'date'
