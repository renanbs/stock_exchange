from django.apps import AppConfig


class StockQuotationConfig(AppConfig):
    name = 'stock_quotation'
