"""stock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework import routers

from stock.stock_quotation.views import CompanyViewSet

from django.conf.urls import url, include

from . import views

router = routers.DefaultRouter()
router.register(r'company', views.CompanyViewSet)
router.register(r'order_company', views.OrderCompanyViewSet)
router.register(r'stock', views.StockViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
app_name = 'stock_quotation'
urlpatterns = [
    # url(r'^stocks2', StockListView.as_view(), name="stocks2")
]

urlpatterns += router.urls
