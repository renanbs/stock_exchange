import os

import requests


class AlphaAdvantageStock:
    api_key = os.getenv('ALPHA_ADVANTAGE_API_KEY', 'demo')
    base_url = 'https://www.alphavantage.co/'

    @staticmethod
    def _parse_data(data):
        stocks = list()
        error = data.get('Error Message', None)
        if error is None:
            series = data.get('Time Series (Daily)', None)
            if series is not None:
                for key, value in series.items():
                    stock = {
                        'date': key,
                        'open': value.get('1. open')[:-1],
                        'high': value.get('2. high')[:-1],
                        'low': value.get('3. low')[:-1],
                        'close': value.get('4. close')[:-1],
                        'volume': value.get('5. volume')
                    }
                    stocks.append(stock)
            return stocks, 'ok'
        return [], error

    def get_daily_series(self, symbol, output_size='compact'):
        url = self.base_url + 'query?function=TIME_SERIES_DAILY&symbol={}.SA&outputsize={}&apikey={}'
        url = url.format(symbol, output_size, self.api_key)
        response = requests.get(url)
        if response.status_code == 200:
            data, msg = self._parse_data(response.json())
            return data, msg
        return [], 'empty'
