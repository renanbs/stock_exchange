# Generated by Django 2.0.4 on 2018-05-10 02:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock_quotation', '0008_company_last_negotiation_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='last_negotiation_date',
            field=models.DateField(blank=True),
        ),
    ]
