from rest_framework import serializers

from .models import Company, FundamentalistIndicator, OrderCompany, Stock


class FundamentalistIndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = FundamentalistIndicator
        fields = ('id', 'release_date', 'stock_no', 'pl', 'lpa', 'pvp', 'vpa', 'pebit', 'mb',
                  'psr', 'me', 'at', 'ml',
                  'pcg', 'ebita', 'p_ativ_circ_liq', 'roic', 'div_yield', 'roe', 'ev_ebit', 'liquidez_corr',
                  'giro_ativ', 'div_br_patrim', 'cresc_rec_5_a', 'ativo', 'div_bruta', 'disponibilidades',
                  'div_liquida', 'ativo_circulante', 'patr_liquido')


class CompanyCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('code',)

    def create(self, validated_data):
        company_in_db = Company.objects.filter(code=validated_data.get('code')).first()
        if not company_in_db:
            Company.objects.create(**validated_data)


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ('id', 'date', 'open', 'high', 'low', 'close', 'volume')

    def create(self, validated_data):
        date = validated_data.get('date')
        company = validated_data.get('company')
        if not Stock.objects.filter(date=date, company=company).exists():
            stock = Stock.objects.create(**validated_data)
            return stock
        return validated_data


class CompanySerializer(serializers.ModelSerializer):
    indicators = FundamentalistIndicatorSerializer(many=True)

    class Meta:
        model = Company
        fields = ('id', 'code', 'name', 'paper_type', 'sector', 'sub_sector', 'last_negotiation_date',
                  'indicators', 'disable')

    def create(self, validated_data):
        indicator_data = validated_data.pop('indicators')
        company = Company.objects.create(**validated_data)
        for track_data in indicator_data:
            FundamentalistIndicator.objects.create(company=company, **track_data)
        return company

    def update(self, instance, validated_data):
        indicator_data = validated_data.pop('indicators')

        instance.code = validated_data.get('code', instance.code)
        instance.name = validated_data.get('name', instance.name)
        instance.paper_type = validated_data.get('paper_type', instance.paper_type)
        instance.sector = validated_data.get('sector', instance.sector)
        instance.sub_sector = validated_data.get('sub_sector', instance.sub_sector)
        instance.last_negotiation_date = validated_data.get('last_negotiation_date', instance.last_negotiation_date)
        instance.disable = validated_data.get('disable', instance.disable)
        instance.save()
        for indicator in indicator_data:
            release_date = indicator.get('release_date', None)
            indicator_in_db = FundamentalistIndicator.objects.filter(release_date=release_date,
                                                                     company__code=instance.code).first()
            if not indicator_in_db:
                FundamentalistIndicator.objects.create(company=instance, **indicator)
        return instance


class ResponseCompanySerializer(CompanySerializer):
    last_indicator = FundamentalistIndicatorSerializer()
    last_stock = StockSerializer()
    last_stock_month = serializers.SerializerMethodField()

    def get_last_stock_month(self, obj):
        return StockSerializer(obj.last_stock_month, many=True).data

    class Meta:
        model = Company
        fields = ('id', 'code', 'name', 'paper_type', 'sector', 'sub_sector', 'last_negotiation_date',
                  'last_indicator', 'last_stock_month', 'last_stock', 'disable')


class OrderCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderCompany
        fields = ('id', 'name', 'pl', 'disable_pl', 'lpa', 'disable_lpa', 'pvp', 'disable_pvp', 'vpa', 'disable_vpa',
                  'pebit', 'disable_pebit', 'mb', 'disable_mb', 'psr', 'disable_psr', 'me', 'disable_me',
                  'at', 'disable_at', 'ml', 'disable_ml', 'pcg', 'disable_pcg', 'ebita', 'disable_ebita',
                  'p_ativ_circ_liq', 'disable_p_ativ_circ_liq', 'roic', 'disable_roic',
                  'div_yield', 'disable_div_yield', 'roe', 'disable_roe', 'ev_ebit', 'disable_ev_ebit',
                  'liquidez_corr', 'disable_liquidez_corr', 'giro_ativ', 'disable_giro_ativ',
                  'div_br_patrim', 'disable_div_br_patrim', 'cresc_rec_5_a', 'disable_cresc_rec_5_a',
                  'ativo', 'disable_ativo', 'div_bruta', 'disable_div_bruta',
                  'disponibilidades', 'disable_disponibilidades',
                  'div_liquida', 'disable_div_liquida', 'ativo_circulante', 'disable_ativo_circulante',
                  'patr_liquido', 'disable_patr_liquido')
